#ifndef CAR_COLOR_CLASSIFY
#define CAR_COLOR_CLASSIFY

#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "std_msgs/Float32MultiArray.h"
#include <iostream>
#include "sensor_msgs/Image.h"

using namespace std;
using namespace cv;

const Scalar hsvRedLo(141,  52,  45);  
const Scalar hsvRedHi(255, 151, 254);  
const Scalar hsvWhiteLo(105, 25, 70);//110, 64, 69  
const Scalar hsvWhiteHi(120, 135, 255);  //120, 140, 254
const Scalar hsvBlueLo(86,  90,  53);  
const Scalar hsvBlueHi(120, 170, 220);//120, 170, 253 


class color_classify
{
    ros::NodeHandle nh_;
    image_transport::ImageTransport it_;
    ros::Subscriber image_sub_;
    ros::Subscriber bbox_sub_;
    image_transport::Publisher image_pub_;
    ros::Publisher car_color_pub_;
    cv::Mat image;
    int color_index_last, color_index_output, color_filter_count, color_new;
    

  public:
    color_classify();
    void imageCb(const sensor_msgs::CompressedImageConstPtr& msg);
    void bboxCallback(const std_msgs::Float32MultiArray::ConstPtr& msg);
};

#endif