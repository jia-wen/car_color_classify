#include "car_color_classify/car_color_classify.h"

// static const std::string OPENCV_WINDOW = "Image window";


color_classify::color_classify(): it_(nh_)
{
  bbox_sub_ = nh_.subscribe("/car_tracking2", 100, &color_classify::bboxCallback, this);
  image_sub_ = nh_.subscribe("/camera/image_rect_color/compressed", 1, &color_classify::imageCb, this);
  image_pub_ = it_.advertise("/image_converter/output", 1);
  car_color_pub_ = nh_.advertise<std_msgs::Float32MultiArray>("/car_color", 1000);
  // image_sub_ = it_.subscribe("/usb_cam/image_raw", 1, &color_classify::imageCb, this);
  color_index_last = -1;
}


void color_classify::imageCb(const sensor_msgs::CompressedImageConstPtr& msg)
{
  cv_bridge::CvImagePtr cv_ptr;
  try
  {
    cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
  }
  catch (cv_bridge::Exception& e)
  {
    ROS_ERROR("cv_bridge exception: %s", e.what());
    return;
  }
  image = cv_ptr->image;
  // cv_ptr->image = hsv_fg;
  // image_pub_.publish(cv_ptr->toImageMsg());
}

void color_classify::bboxCallback(const std_msgs::Float32MultiArray::ConstPtr& msg)
{
  std::vector<float> bbox_data = msg->data;
  // std::cout<<msg->data.at(2)<< "======"<<msg->data.at(3)<<std::endl;
  // std::cout<<bbox_data[2]<< "======"<<bbox_data[3]<<std::endl;
  std_msgs::Float32MultiArray color_msg;
  // std::cout<<bbox_data.size()<<std::endl;

  for(int bbox_id = 0; bbox_id < bbox_data.size()/6;bbox_id++)
  //for(int bbox_id = 0; bbox_id < bbox_data.size()/4;bbox_id++)
  {
    Rect rt1(bbox_data[bbox_id*6+2], bbox_data[bbox_id*6+3], bbox_data[bbox_id*6+4]-bbox_data[bbox_id*6+2], bbox_data[bbox_id*6+5]-bbox_data[bbox_id*6+3]);
    //Rect rt1(bbox_data[bbox_id*4+2]-100, bbox_data[bbox_id*4+3]-100, 200, 200);
    rt1 &= Rect(0, 0, image.cols, image.rows); 
    // std::cout<<rt1.tl()<< "======"<<rt1.br()<<std::endl;

    cv::Mat crop = Mat::zeros(image.size(), CV_8UC1);
    crop(rt1).setTo(255);
    cv::Mat img_crop;
    
    cv::Mat image_src = image.clone();
    cv::Mat imageROI = image_src(rt1);
    imageROI.convertTo(img_crop, img_crop.type());	

    
    // cv::imshow("img",image);
    // cv::imshow("crop", img_crop);
    // cv::waitKey(1);

    ////////////////////
    cv::Mat hsvImg;

    cvtColor(img_crop, hsvImg, COLOR_BGR2HSV);
    vector<Mat> hsvSplit; 
    split(hsvImg, hsvSplit);  
    equalizeHist(hsvSplit[2],hsvSplit[2]);  
    merge(hsvSplit,hsvImg);

    vector<Scalar> hsvLo;
    hsvLo.push_back(hsvRedLo);
    hsvLo.push_back(hsvWhiteLo);
    hsvLo.push_back(hsvBlueLo);

    vector<Scalar> hsvHi;
    hsvHi.push_back(hsvRedHi);
    hsvHi.push_back(hsvWhiteHi);
    hsvHi.push_back(hsvBlueHi);

    vector<std::string> car_color;
    car_color.push_back("Red");
    car_color.push_back("White");
    car_color.push_back("Blue");

    cv::Mat fg_mask;
    cv::Mat bg_mask;
    cv::Mat hsv_fg;
    cv::Mat fg_range_mask;
    cv::Mat car_color_mask;
    cv::Mat hsv_fg_in_range;
    cv::Mat car_color_img;
    
    Scalar mask_sum;
    int maxsum;
    int color_index = 0;
    String color;

    maxsum = 10;
    for(int colorIdx = 0; colorIdx < 3; colorIdx++)
    {
      inRange(hsvImg, Scalar(0, 14, 30), Scalar(70, 112, 213),bg_mask);  //new_bg
      threshold(bg_mask, fg_mask, 1, 255, THRESH_BINARY_INV);  // fg

      Mat element = getStructuringElement(MORPH_RECT, Size(5, 5));  
      morphologyEx(fg_mask, fg_mask, MORPH_OPEN, element); 
      morphologyEx(fg_mask, fg_mask, MORPH_CLOSE, element);

      hsvImg.copyTo(hsv_fg,fg_mask);
      inRange(hsv_fg, Scalar( 0,  0,  25), Scalar( 180,  255,  253), fg_range_mask);
      threshold(fg_range_mask, fg_range_mask, 1, 255, THRESH_BINARY);
      hsv_fg.copyTo(hsv_fg_in_range,fg_range_mask);///////////////////////////////////hsvImg?

      inRange(hsv_fg_in_range, hsvLo[colorIdx], hsvHi[colorIdx], car_color_mask);
      threshold(car_color_mask, car_color_mask, 1, 255, THRESH_BINARY);
      img_crop.copyTo(car_color_img,car_color_mask);

      // // cv::imshow("img",img_crop);
      // // cv::imshow("hsv",hsvImg);
      // cv::imshow("hsv_fg_in_range",hsv_fg_in_range);
      // cv::imshow("fg_mask",fg_mask);
      // cv::imshow("crop_hsv",hsvImg);
      // cv::imshow("fg_range_mask",fg_range_mask);
      // char *win_name = const_cast<char *>(car_color[colorIdx].c_str()) ;
      // cv::imshow(win_name,car_color_img);
      // cv::waitKey(1);
      

      mask_sum = 0;
      mask_sum = sum(car_color_mask);
      // std::cout<<colorIdx<<"***"<<mask_sum[0]<<std::endl;
      if(mask_sum[0] > maxsum)
      {
        maxsum = mask_sum[0];
        color = car_color[colorIdx];
        color_index = colorIdx;
      }
    }
    double probability = 0;
    if(maxsum==10)
    {
      color_index = 0;  // background
    }
    else
    {
      color_index = color_index+1;
      probability = sum(car_color_mask)[0] / sum(fg_range_mask)[0];
    }

    // std::cout<<maxsum<<std::endl;
    // std::cout<<color<<std::endl;
    // std::cout<<"--------"<<std::endl;
    

    if(bbox_data.size()/6==1) // only filter one bbox case
    {
      if(color_index!=color_index_last)  //color_index_last -1
      {
          // if(color_filter_count == 0)
          // {
          //   color_new = color_index;
          // }
          // else
          // {
          //   if(color_new!=color_index){color_filter_count = 0;}
          // }
          color_filter_count ++;
          if(color_filter_count>2)
          {
            color_index_output = color_index;
            color_index_last = color_index;
          }
      }
      else
      {
        color_filter_count = 0;
      }
    }
    else
    {
      color_index_output = color_index;
    }
    // color_index_output = color_index;
    color_msg.data.push_back(color_index_output);
    color_msg.data.push_back(probability);
  }
  car_color_pub_.publish(color_msg);
}


int main(int argc, char** argv)
{
  ros::init(argc, argv, "car_color_classify");
  color_classify cc;
  ros::spin();
  return 0;
}
